port module Main exposing (..)

import Angle
import AngularSpeed exposing (AngularSpeed)
import Axis3d
import Block3d
import Browser
import Browser.Events exposing (Visibility(..))
import Camera3d exposing (Camera3d)
import Circle3d
import Color exposing (Color)
import Coordinates exposing (Coordinates)
import Cylinder3d
import Dict exposing (Dict)
import Direction2d exposing (Direction2d)
import Direction3d
import Duration
import Enemy exposing (Enemy)
import Html exposing (Html)
import Html.Attributes as Attr
import Illuminance
import Json.Decode as Dec
import Length exposing (Length)
import Levels exposing (LevelSpecification)
import Luminance
import LuminousFlux
import Pixels
import Plane3d
import Point2d exposing (Point2d)
import Point3d
import Quantity exposing (Quantity)
import Random
import RemoteData exposing (RemoteData(..))
import Result
import Scene3d exposing (Entity)
import Scene3d.Light as Light
import Scene3d.Material as Material exposing (Texture)
import Set exposing (Set)
import SketchPlane3d
import Speed exposing (Speed)
import Sphere3d
import Task
import Time
import Vector3d
import Viewpoint3d
import WebGL.Texture


type WalkingState
    = Forwards
    | Backwards
    | NoWalking


type TurningState
    = Left
    | Right
    | NoTurning


type GameState
    = Home
    | Playing
    | Dead
    | Victory


type alias Model =
    { pressedKeys : Set String
    , position : Point2d Length.Meters Coordinates
    , direction : Direction2d Coordinates
    , walking : WalkingState
    , turning : TurningState
    , levels : Dict Int Level
    , currentLevel : Int
    , state : GameState
    }


type alias Level =
    { texture : RemoteData WebGL.Texture.Error (Texture Color)
    , enemies : List Enemy
    , size : Float
    , name : String
    , gatePosition : Point2d Length.Meters Coordinates
    }


init : ( Model, Cmd Msg )
init =
    ( { pressedKeys = Set.empty
      , position = Point2d.origin
      , direction = Direction2d.from Point2d.origin (Point2d.meters -40 40) |> Maybe.withDefault Direction2d.positiveY
      , walking = NoWalking
      , turning = NoTurning
      , levels = initLevels Levels.levels
      , currentLevel = 0
      , state = Home
      }
    , loadLevelsData Levels.levels
    )


initLevels : List LevelSpecification -> Dict Int Level
initLevels levelsSpecs =
    levelsSpecs
        |> List.map initLevel
        |> Dict.fromList


initLevel : LevelSpecification -> ( Int, Level )
initLevel spec =
    ( spec.id
    , { emptyLevel
        | gatePosition = spec.gatePosition
        , size = spec.size
        , name = spec.name
      }
    )


loadLevelData : LevelSpecification -> Cmd Msg
loadLevelData spec =
    Cmd.batch
        [ Material.load
            ("%PUBLIC_URL%/img/" ++ spec.textureImg)
            |> Task.attempt (RemoteData.fromResult >> GotTexture spec.id)
        , Random.generate (GotInitialEnemies spec.id) (initialEnemies spec.enemyCount)
        ]


loadLevelsData : List LevelSpecification -> Cmd Msg
loadLevelsData listSpecs =
    listSpecs
        |> List.map loadLevelData
        |> Cmd.batch


type Msg
    = GotFrameDelta Float
    | GotTexture Int (RemoteData WebGL.Texture.Error (Texture Color))
    | KeyDown String
    | KeyUp String
    | KeysChanged
    | VisibilityChanged Visibility
    | GotInitialEnemies Int (List Enemy)
    | ChangeEnemiesDirection Time.Posix
    | NextLevel


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        KeysChanged ->
            ( { model
                | walking =
                    case ( Set.member "ArrowUp" model.pressedKeys, Set.member "ArrowDown" model.pressedKeys ) of
                        ( True, False ) ->
                            Forwards

                        ( False, True ) ->
                            Backwards

                        _ ->
                            NoWalking
                , turning =
                    case ( Set.member "ArrowLeft" model.pressedKeys, Set.member "ArrowRight" model.pressedKeys ) of
                        ( True, False ) ->
                            Left

                        ( False, True ) ->
                            Right

                        _ ->
                            NoTurning
              }
            , Cmd.none
            )

        KeyDown key ->
            if key == "Enter" then
                case model.state of
                    Home ->
                        ( { model | state = Playing }, Cmd.none )

                    Dead ->
                        ( resetGame model, loadLevelsData Levels.levels )

                    _ ->
                        ( model, Cmd.none )

            else
                { model | pressedKeys = Set.insert key model.pressedKeys } |> update KeysChanged

        KeyUp key ->
            { model | pressedKeys = Set.remove key model.pressedKeys } |> update KeysChanged

        VisibilityChanged visibility ->
            case visibility of
                Hidden ->
                    { model | pressedKeys = Set.empty } |> update KeysChanged

                _ ->
                    ( model, Cmd.none )

        GotTexture levelId result ->
            case result of
                Success texture ->
                    ( updateLevel levelId (\level -> { level | texture = Success texture }) model, Cmd.none )

                _ ->
                    ( model, Cmd.none )

        GotInitialEnemies levelId enemies ->
            ( updateLevel levelId (\level -> { level | enemies = enemies }) model, Cmd.none )

        ChangeEnemiesDirection time ->
            ( updateCurrentLevel (\level -> { level | enemies = changeEnemiesDirection time level.enemies }) model
            , Cmd.none
            )

        NextLevel ->
            ( nextLevel model, Cmd.none )

        GotFrameDelta delta ->
            ( if model.state == Playing then
                model
                    |> movePlayer delta
                    |> moveEnemies delta
                    |> checkCollisionsEnemies
                    |> checkCollisionsWalls
                    |> checkAndPassToNextLevel

              else
                model
            , Cmd.none
            )


emptyLevel : Level
emptyLevel =
    { texture = NotAsked
    , enemies = []
    , size = 0
    , name = ""
    , gatePosition = Point2d.meters 42 42
    }


nextLevel : Model -> Model
nextLevel model =
    if model.currentLevel < (Dict.size model.levels - 1) then
        { model
            | currentLevel = model.currentLevel + 1
            , direction = Direction2d.from model.position Point2d.origin |> Maybe.withDefault model.direction
        }

    else
        { model | state = Victory }


checkAndPassToNextLevel : Model -> Model
checkAndPassToNextLevel model =
    if
        Point2d.equalWithin
            (Length.meters 0.5)
            model.position
            (currentLevel model).gatePosition
    then
        nextLevel model

    else
        model


resetGame : Model -> Model
resetGame model =
    { model
        | state = Playing
        , currentLevel = 0
        , position = Point2d.origin
        , direction = Direction2d.from Point2d.origin (Point2d.meters -40 40) |> Maybe.withDefault Direction2d.positiveY
        , pressedKeys = Set.empty
        , walking = NoWalking
        , turning = NoTurning
    }


checkCollisionsEnemies : Model -> Model
checkCollisionsEnemies model =
    let
        level =
            currentLevel model
    in
    { model
        | state =
            if List.any (\enemy -> Point2d.equalWithin (Length.meters 0.1) enemy.position model.position) level.enemies then
                Dead

            else
                model.state
    }


checkCollisionsWalls : Model -> Model
checkCollisionsWalls model =
    let
        limit =
            (currentLevel model).size
                |> Length.meters
                |> Quantity.minus playerRadius

        correctPosition =
            model.position
                |> checkNorthWall limit
                |> checkSouthWall limit
                |> checkWestWall limit
                |> checkEastWall limit
    in
    { model | position = correctPosition }


checkNorthWall : Length -> Point2d Length.Meters Coordinates -> Point2d Length.Meters Coordinates
checkNorthWall limit position =
    if Point2d.yCoordinate position |> Quantity.greaterThan limit then
        Point2d.xy (Point2d.xCoordinate position) limit

    else
        position


checkSouthWall : Length -> Point2d Length.Meters Coordinates -> Point2d Length.Meters Coordinates
checkSouthWall limit position =
    if Point2d.yCoordinate position |> Quantity.lessThan (Quantity.negate limit) then
        Point2d.xy (Point2d.xCoordinate position) (Quantity.negate limit)

    else
        position


checkEastWall : Length -> Point2d Length.Meters Coordinates -> Point2d Length.Meters Coordinates
checkEastWall limit position =
    if Point2d.xCoordinate position |> Quantity.greaterThan limit then
        Point2d.xy limit (Point2d.yCoordinate position)

    else
        position


checkWestWall : Length -> Point2d Length.Meters Coordinates -> Point2d Length.Meters Coordinates
checkWestWall limit position =
    if Point2d.xCoordinate position |> Quantity.lessThan (Quantity.negate limit) then
        Point2d.xy (Quantity.negate limit) (Point2d.yCoordinate position)

    else
        position


enemyGenerator : Random.Generator Enemy
enemyGenerator =
    Random.map2
        (\x y -> { position = Point2d.meters x y, direction = Direction2d.x })
        (Random.float -20 20)
        (Random.float -20 20)


initialEnemies : Int -> Random.Generator (List Enemy)
initialEnemies count =
    Random.list count enemyGenerator


changeEnemiesDirection : Time.Posix -> List Enemy -> List Enemy
changeEnemiesDirection time enemies_ =
    let
        anglesGenerator =
            Random.float -60 60
                |> Random.map Angle.degrees
                |> Random.list (List.length enemies_)

        ( angles, _ ) =
            Random.step anglesGenerator (time |> Time.posixToMillis |> Random.initialSeed)
    in
    List.map2
        (\angle enemy_ -> { enemy_ | direction = Direction2d.fromAngle (Quantity.plus (Direction2d.toAngle enemy_.direction) angle) })
        angles
        enemies_


playerSpeed : Speed
playerSpeed =
    Length.kilometers 30 |> Quantity.per Duration.hour


enemySpeed : Speed
enemySpeed =
    Length.kilometers 10 |> Quantity.per Duration.hour


playerRotationSpeed : AngularSpeed
playerRotationSpeed =
    AngularSpeed.turnsPerSecond 0.5


movePlayer : Float -> Model -> Model
movePlayer delta model =
    let
        position =
            newPosition
                delta
                (case model.walking of
                    Forwards ->
                        playerSpeed

                    Backwards ->
                        Quantity.negate playerSpeed

                    _ ->
                        Quantity.zero
                )
                model.direction
                model.position

        direction =
            updateDirection
                delta
                (case model.turning of
                    Left ->
                        playerRotationSpeed

                    Right ->
                        Quantity.negate playerRotationSpeed

                    _ ->
                        Quantity.zero
                )
                model.direction
    in
    { model | position = position, direction = direction }


moveEnemies : Float -> Model -> Model
moveEnemies delta model =
    updateCurrentLevel
        (\level ->
            { level
                | enemies =
                    List.map
                        (\enemy ->
                            if
                                Point2d.distanceFrom model.position enemy.position
                                    |> Quantity.lessThan (Length.meters 5)
                            then
                                let
                                    towardsPlayer =
                                        Direction2d.from enemy.position model.position
                                            |> Maybe.withDefault enemy.direction
                                in
                                { enemy | position = newPosition delta enemySpeed towardsPlayer enemy.position }

                            else
                                { enemy | position = newPosition delta enemySpeed enemy.direction enemy.position }
                        )
                        level.enemies
            }
        )
        model


updateCurrentLevel : (Level -> Level) -> Model -> Model
updateCurrentLevel updateFn model =
    updateLevel model.currentLevel updateFn model


updateLevel : Int -> (Level -> Level) -> Model -> Model
updateLevel levelId updateFn model =
    let
        level =
            Dict.get levelId model.levels
                |> Maybe.withDefault emptyLevel

        updatedLevels =
            Dict.insert levelId (updateFn level) model.levels
    in
    { model | levels = updatedLevels }


newPosition : Float -> Speed -> Direction2d coordinates -> Point2d Length.Meters coordinates -> Point2d Length.Meters coordinates
newPosition delta speed direction position =
    let
        distance =
            speed |> Quantity.for (Duration.milliseconds delta)
    in
    Point2d.translateIn direction distance position


updateDirection : Float -> AngularSpeed -> Direction2d coordinates -> Direction2d coordinates
updateDirection delta angularSpeed direction =
    let
        angle =
            angularSpeed |> Quantity.for (Duration.milliseconds delta)
    in
    Direction2d.rotateBy angle direction


renderTerrain : Level -> Bool -> List (Entity Length.Meters)
renderTerrain level outside =
    let
        terrainSize =
            level.size

        wallsHeight =
            3

        slopeOffset =
            if outside then
                30

            else
                6

        material =
            case level.texture of
                Success texture_ ->
                    Material.texturedMatte texture_

                _ ->
                    Material.texturedMatte (Material.constant Color.darkGray)

        baseWall =
            Scene3d.quad
                material
                (Point3d.meters (negate terrainSize) 0 0)
                (Point3d.meters (negate terrainSize - slopeOffset) slopeOffset wallsHeight)
                (Point3d.meters (terrainSize + slopeOffset) slopeOffset wallsHeight)
                (Point3d.meters terrainSize 0 0)
                |> (if outside then
                        Scene3d.mirrorAcross Plane3d.xy

                    else
                        identity
                   )
    in
    -- Ground
    [ Scene3d.quad
        material
        (Point3d.meters (negate terrainSize) (negate terrainSize) 0)
        (Point3d.meters terrainSize (negate terrainSize) 0)
        (Point3d.meters terrainSize terrainSize 0)
        (Point3d.meters (negate terrainSize) terrainSize 0)

    -- North wall
    , baseWall |> Scene3d.translateBy (Vector3d.meters 0 terrainSize 0)

    -- West wall
    , baseWall
        |> Scene3d.rotateAround Axis3d.z (Angle.turns 0.25)
        |> Scene3d.translateBy (Vector3d.meters (negate terrainSize) 0 0)

    -- South wall
    , baseWall
        |> Scene3d.rotateAround Axis3d.z (Angle.turns 0.5)
        |> Scene3d.translateBy (Vector3d.meters 0 (negate terrainSize) 0)

    -- East wall
    , baseWall
        |> Scene3d.rotateAround Axis3d.z (Angle.turns -0.25)
        |> Scene3d.translateBy (Vector3d.meters terrainSize 0 0)
    ]


playerRadius : Length
playerRadius =
    Length.meters 0.25


renderPlayer : Model -> Entity Length.Meters
renderPlayer model =
    Scene3d.sphereWithShadow
        (Material.emissive Light.sunlight (Luminance.nits 500000))
        (Sphere3d.atPoint Point3d.origin playerRadius
            |> Sphere3d.translateBy
                (Vector3d.from
                    Point3d.origin
                    (Point3d.on (SketchPlane3d.xy |> SketchPlane3d.offsetBy playerRadius) model.position)
                )
        )


renderEnemies : Level -> List (Entity Length.Meters)
renderEnemies level =
    List.map
        (\enemy ->
            Scene3d.sphereWithShadow
                (if level.name == "Lust" then
                    Material.emissive (Light.color Color.red) (Luminance.nits 10000)

                 else
                    Material.matte Color.darkRed
                )
                (Sphere3d.atPoint Point3d.origin (Length.meters 0.15)
                    |> Sphere3d.translateBy
                        (Vector3d.from
                            Point3d.origin
                            (Point3d.on (SketchPlane3d.xy |> SketchPlane3d.offsetBy (Length.meters 0.5)) enemy.position)
                        )
                )
        )
        level.enemies


renderGate : Model -> List (Entity Length.Meters)
renderGate model =
    let
        level =
            currentLevel model

        gatePosition3d =
            Point3d.on SketchPlane3d.xy level.gatePosition

        block =
            Block3d.from
                gatePosition3d
                (Point3d.translateBy (Vector3d.meters 0.8 0.1 1.5) gatePosition3d)
                |> Block3d.translateBy (Vector3d.meters -0.4 -0.05 0)

        maxRadius =
            Length.meters 0.5

        radius =
            let
                distanceFromGate =
                    Point2d.distanceFrom model.position level.gatePosition

                closestBound =
                    Length.meters 3

                farthestBound =
                    Length.meters 10
            in
            if distanceFromGate |> Quantity.lessThan closestBound then
                maxRadius

            else if distanceFromGate |> Quantity.lessThan farthestBound then
                Quantity.multiplyBy
                    (1
                        - Quantity.ratio
                            (distanceFromGate |> Quantity.minus closestBound)
                            (farthestBound |> Quantity.minus closestBound)
                    )
                    maxRadius

            else
                Quantity.zero
    in
    [ Scene3d.cylinder
        (Material.emissive (Light.color Color.black) (Luminance.nits 400))
        (Cylinder3d.along
            (Axis3d.through gatePosition3d Direction3d.z)
            { start = Quantity.zero
            , end = Length.millimeter
            , radius = radius
            }
        )
    ]


camera : Model -> Camera3d Length.Meters coordinates
camera model =
    let
        playerPosition3d =
            Point3d.on SketchPlane3d.xy model.position
                |> Point3d.translateIn Direction3d.z (Length.meters 1)

        cameraDistance =
            Length.meters 5
    in
    Camera3d.perspective
        { viewpoint =
            Viewpoint3d.lookAt
                { eyePoint =
                    playerPosition3d
                        |> Point3d.translateIn
                            (Direction3d.xyZ
                                (model.direction |> Direction2d.reverse |> Direction2d.toAngle)
                                (Angle.degrees 15)
                            )
                            cameraDistance
                , focalPoint = playerPosition3d
                , upDirection = Direction3d.positiveZ
                }
        , verticalFieldOfView = Angle.degrees 30
        }


lights : Model -> Scene3d.Lights coordinates
lights model =
    Scene3d.threeLights
        (Light.point
            (Light.castsShadows True)
            { chromaticity = Light.sunlight
            , intensity = LuminousFlux.lumens 5000
            , position = Point3d.on (SketchPlane3d.offsetBy (Length.meters 0.25) SketchPlane3d.xy) model.position
            }
        )
        (Light.soft
            { upDirection = Direction3d.z
            , chromaticity = Light.sunlight
            , intensityAbove = Illuminance.lux 100
            , intensityBelow = Illuminance.lux 10
            }
        )
        (Light.directional
            (Light.castsShadows True)
            { chromaticity = Light.sunlight
            , intensity = Illuminance.lux 200
            , direction = Direction3d.negativeZ
            }
        )


entities : Model -> List (Entity Length.Meters)
entities model =
    let
        level =
            currentLevel model

        outside =
            model.currentLevel == 0
    in
    [ renderPlayer model ]
        ++ renderTerrain level outside
        ++ renderGate model
        ++ renderEnemies level


currentLevel : Model -> Level
currentLevel model =
    Dict.get model.currentLevel model.levels
        |> Maybe.withDefault emptyLevel


view : Model -> Html Msg
view model =
    let
        dimensions =
            ( 900, 500 )

        width =
            Attr.style "width" (String.fromInt (Tuple.first dimensions) ++ "px")

        height =
            Attr.style "height" (String.fromInt (Tuple.second dimensions) ++ "px")

        outside =
            model.currentLevel == 0

        screen className contents =
            [ Html.div [ Attr.class className, width, height ] contents ]
    in
    Html.div [ Attr.class "page" ]
        ((case model.state of
            Playing ->
                [ Scene3d.custom
                    { camera = camera model
                    , clipDepth = Length.meters 1
                    , background =
                        Scene3d.backgroundColor
                            (if outside then
                                Color.rgb255 25 25 112

                             else
                                Color.black
                            )
                    , dimensions = Tuple.mapBoth Pixels.pixels Pixels.pixels dimensions
                    , exposure = Scene3d.exposureValue 6
                    , toneMapping = Scene3d.noToneMapping
                    , whiteBalance = Light.sunlight
                    , lights = lights model
                    , antialiasing = Scene3d.multisampling
                    , entities = entities model
                    }
                , Html.div [ Attr.class "hud", width, height ]
                    [ Html.text (currentLevel model).name ]
                ]

            Victory ->
                screen "victory"
                    [ Html.h1 [] [ Html.text "Yay!" ]
                    , Html.p []
                        [ Html.text "You made it through the Purgatory to Heaven."
                        , Html.br [] []
                        , Html.text "(we skipped the boring parts)"
                        ]
                    ]

            Home ->
                screen "home"
                    [ Html.h1 [] [ Html.text "Ludum Dante’s Inferno" ]
                    , Html.p []
                        [ Html.text "Use the arrow keys to move."
                        , Html.br [] []
                        , Html.text "Press Enter to start."
                        ]
                    , Html.p [ Attr.class "inscription" ] [ Html.text "“Abandon all hope, ye who enter here.”" ]
                    ]

            Dead ->
                screen "dead"
                    [ Html.h1 [] [ Html.text "Dead" ]
                    , Html.p []
                        [ Html.text "You died in Hell."
                        , Html.br [] []
                        , Html.text "Press Enter to start again."
                        ]
                    ]
         )
            ++ [ Html.div
                    [ Attr.class "overlay"
                    , width
                    , height
                    ]
                    []
               ]
        )


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch
        [ Browser.Events.onAnimationFrameDelta GotFrameDelta
        , Browser.Events.onKeyDown (Dec.field "key" Dec.string |> Dec.map KeyDown)
        , Browser.Events.onKeyUp (Dec.field "key" Dec.string |> Dec.map KeyUp)
        , Browser.Events.onVisibilityChange VisibilityChanged
        , onBlurChange
            (\blurred ->
                VisibilityChanged
                    (if blurred then
                        Hidden

                     else
                        Visible
                    )
            )
        , Time.every 50 ChangeEnemiesDirection
        ]


port onBlurChange : (Bool -> msg) -> Sub msg


main : Program () Model Msg
main =
    Browser.element
        { view = view
        , init = \_ -> init
        , update = update
        , subscriptions = subscriptions
        }
