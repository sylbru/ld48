import './main.css';
import { Elm } from './Main.elm';
import * as serviceWorker from './serviceWorker';

const app = Elm.Main.init({
  node: document.getElementById('root')
});

serviceWorker.unregister();

window.addEventListener("blur", event => {
  app.ports.onBlurChange.send(true)
})

window.addEventListener("focus", event => {
  app.ports.onBlurChange.send(false)
})