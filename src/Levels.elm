module Levels exposing (..)

import Coordinates exposing (Coordinates)
import Length
import Point2d exposing (Point2d)


type alias LevelSpecification =
    { id : Int
    , name : String
    , textureImg : String
    , size : Float
    , enemyCount : Int
    , gatePosition : Point2d Length.Meters Coordinates
    }


levels : List LevelSpecification
levels =
    [ forest, limbo, lust, gluttony, greed, wrath, heresy, violence, fraud, treachery ]


forest : LevelSpecification
forest =
    { id = 0
    , name = "Forest"
    , textureImg = "forest.png"
    , size = 50
    , enemyCount = 0
    , gatePosition = Point2d.meters -38 35
    }


limbo : LevelSpecification
limbo =
    { id = 1
    , name = "Limbo"
    , textureImg = "grey.png"
    , size = 25
    , enemyCount = 5
    , gatePosition = Point2d.meters -7 7
    }


lust : LevelSpecification
lust =
    { id = 2
    , name = "Lust"
    , textureImg = "darkred.png"
    , size = 22.5
    , enemyCount = 10
    , gatePosition = Point2d.meters -12 -1
    }


gluttony : LevelSpecification
gluttony =
    { id = 3
    , name = "Gluttony"
    , textureImg = "pink.png"
    , size = 20
    , enemyCount = 20
    , gatePosition = Point2d.meters 8 3
    }


greed : LevelSpecification
greed =
    { id = 4
    , name = "Greed"
    , textureImg = "gold.png"
    , size = 17.5
    , enemyCount = 20
    , gatePosition = Point2d.meters 8 -9
    }


wrath : LevelSpecification
wrath =
    { id = 5
    , name = "Wrath"
    , textureImg = "brightred.png"
    , size = 15
    , enemyCount = 15
    , gatePosition = Point2d.meters 0 -2
    }


heresy : LevelSpecification
heresy =
    { id = 6
    , name = "Heresy"
    , textureImg = "bluegreen.png"
    , size = 12.5
    , enemyCount = 20
    , gatePosition = Point2d.meters 9 -9
    }


violence : LevelSpecification
violence =
    { id = 7
    , name = "Violence"
    , textureImg = "black.png"
    , size = 10
    , enemyCount = 20
    , gatePosition = Point2d.meters 4 -1
    }


fraud : LevelSpecification
fraud =
    { id = 8
    , name = "Fraud"
    , textureImg = "purple.png"
    , size = 7.5
    , enemyCount = 30
    , gatePosition = Point2d.meters -3 2
    }


treachery : LevelSpecification
treachery =
    { id = 9
    , name = "Treachery"
    , textureImg = "darkblue.png"
    , size = 5
    , enemyCount = 30
    , gatePosition = Point2d.meters 0 0
    }
