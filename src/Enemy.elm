module Enemy exposing (..)

import Coordinates exposing (Coordinates)
import Direction2d exposing (Direction2d)
import Length exposing (Length)
import Point2d exposing (Point2d)


type alias Enemy =
    { direction : Direction2d Coordinates
    , position : Point2d Length.Meters Coordinates
    }
